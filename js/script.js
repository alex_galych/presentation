/**
 * Created with JetBrains PhpStorm.
 * User: Alexander Galych
 * email: galych@zfort.com
 * Date: 7/24/13
 * Time: 12:04 PM
 * To change this template use File | Settings | File Templates.
 */



//Using jQuery

$(document).ready(function() {

    $('#article').triggerHandler('resize');

    var presentationArray = new Array();
    presentationArray['Presentation1'] = new Array('Slide 11', 'Slide 12', 'Slide 13', 'Slide 14', 'Slide 15');
    presentationArray['Presentation2'] = new Array('Slide 21', 'Slide 22', 'Slide 23', 'Slide 24', 'Slide 25');
    presentationArray['Presentation3'] = new Array('Slide 31', 'Slide 32', 'Slide 33', 'Slide 34', 'Slide 35');
    presentationArray['Presentation4'] = new Array('Slide 41', 'Slide 42', 'Slide 43', 'Slide 44', 'Slide 45');


    $('.presentation-content').presentations({
        'arraySlides' : presentationArray,
        'showSlide': 'Presentation1'
    });


});