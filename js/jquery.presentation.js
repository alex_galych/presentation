/**
 * Created with JetBrains PhpStorm.
 * User: galych
 * Date: 7/25/13
 * Time: 12:04 PM
 * To change this template use File | Settings | File Templates.
 */
(function( $ ) {
    $.fn.presentations = function(options) {
        var settings = $.extend( {
            'arrayslides': new Array(),
            'createButton': '.new-slide-button',
            'createInput': '#new-slide-input-id',
            'createNewpresentation' : '.new-presentation-button',
            'presentationName': '',
            'showslide': ''
        }, options);

        var presentationObj = this;
        var methods = {
            init : function( settings ,handleObj) {
                var html =
                    '<div class="top">' +
                        '<div class="name">Presentations</div>'+
                        '<div class="hide-all"></div>' +
                    '</div> '+
                    '<div class="bottom">'+
                        '<div class="accordion-container">';
                for (var key in settings['arraySlides']) {
                    html += '<div class="presentation" style="'+ (( settings['showSlide'] == key )?'background-color: #EFEFEF;':'') +'">\n\
                                <div class="presentation-name">\
                                    <div class="name">'+key+'</div>\n\
                                    <div class="presentation-name-controls">\n\
                                        <div class="presentation-name-controls-done"><img src="img/done.png"></div>\n\
                                        <div class="presentation-name-controls-edit"><img src="img/edit.png"></div>\n\
                                        <div class="presentation-name-controls-delete"><img src="img/delete.png"></div>\n\
                                    </div>';
                    if(settings['showSlide'] == key){
                        html += '<div class="hide-presentation"></div>'+
                            '</div>'+
                            '<div class="presentation-slides">';
                    }else{
                        html += '<div class="show-presentation"></div>'+
                            '</div>'+
                            '<div class="presentation-slides" style="display: none">';
                    }
                    html += '<div class="presentation-container"><div class="slides">';

                    $.each(settings['arraySlides'][key], function( key1, value1 ) {
                        html += '<div class="presentation-slide">\n\
                            <div class="presentation-slide-text">'+value1+'</div>\n\
                            <div class="presentation-slide-controls">\n\
                                <div class="presentation-slide-controls-done"><img src="img/done.png"></div>\n\
                                <div class="presentation-slide-controls-edit"><img src="img/edit.png"></div>\n\
                                <div class="presentation-slide-controls-delete"><img src="img/delete.png"></div>\n\
                            </div>\n\
                         </div>';
                    });

                    html += '</div>\n\
                    <div class="presentation-window">\
                        <div class="window"></div>\n\
                        <div class="controls">\n\
                            <div class="controls-buttons">\n\
                                <button>Prev</button>\n\
                                <button>Play</button>\n\
                                <button>Pause</button>\n\
                                <button>Next</button>\n\
                            </div>\n\
                        <div class="controls-slider"></div>\n\
                        </div>\n\
                    </div>\n\
                    </div>\n\
                            <div class="new-slide">\n\
                                <div class="new-slide-button">Add Slide</div>\n\
                                <div class="new-slide-input">\n\
                                    <input type="text" maxlength="50" />\n\
                                </div>\n\
                            </div>\n\
                        </div>\n\
                    </div>';
                }

                html += '</div>'+
                            '<div class="new-presentation">'+
                                '<div class="new-presentation-button">Add Presentation</div>'+
                                '<div class="new-presentation-input">'+
                                    '<input type="text" maxlength="50">'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                    '</div>';

                $(handleObj).html(html);
            },

            add: function(slideName ,handleObj){
                var html = '<div class="presentation-slide">\n\
                        <div class="presentation-slide-text">'+slideName+'</div>\n\
                        <div class="presentation-slide-controls">\n\
                            <div class="presentation-slide-controls-done"><img src="img/done.png"></div>\n\
                            <div class="presentation-slide-controls-edit"><img src="img/edit.png"></div>\n\
                            <div class="presentation-slide-controls-delete"><img src="img/delete.png"></div>\n\
                        </div>\n\
                     </div>';
                $(handleObj).append(html);
            },

            addPresentation: function(slideName ,handleObj){
                var html = '<div class="presentation">\n\
                                <div class="presentation-name">\
                                    <div class="name">'+slideName+'</div>\n\
                                    <div class="presentation-name-controls">\n\
                                        <div class="presentation-name-controls-done"><img src="img/done.png"></div>\n\
                                        <div class="presentation-name-controls-edit"><img src="img/edit.png"></div>\n\
                                        <div class="presentation-name-controls-delete"><img src="img/delete.png"></div>\n\
                                    </div>\n\
                                    <div class="show-presentation"></div>\n\
                                </div>\n\
                                <div class="presentation-slides" style="display: none;">\n\
                                    <div class="presentation-container">\n\
                                        <div class="slides"></div>\n\
                                        <div class="presentation-window"></div>\n\
                                    </div>\n\
                                    <div class="new-slide">\n\
                                        <div class="new-slide-button">Add New</div>\n\
                                        <div class="new-slide-input">\n\
                                            <input type="text" maxlength="50" />\n\
                                        </div>\n\
                                    </div>\n\
                                </div>\n\
                            </div>';
                $(handleObj).append(html);
            },

            edit: function( handleObj, element) {
                var value = $(handleObj).parent().parent().find(element).html();
                if(element == '.presentation-slide-text'){
                    var doneBtn = $(handleObj).parent().find('.presentation-slide-controls-done');
                }else{
                    var doneBtn = $(handleObj).parent().find('.presentation-name-controls-done');

                }
                $(handleObj).hide();
                $(doneBtn).show();
                $(handleObj).parent().parent().find(element).html('<input type="text" value="'+value+'" maxlength="50"/>');
            },

            deleteSlide: function( handleObj,type) {
                if(type == '.presentation-slide-text' ){
                    $(handleObj).parent().parent().remove();
                }else{
                    $(handleObj).parent().parent().parent().remove();
                }
            },

            done: function( handleObj, element) {
                var value = $(handleObj).parent().parent().find(element+' :input').val();
                if( element == '.presentation-slide-text'){
                    var editBtn = $(handleObj).parent().find('.presentation-slide-controls-edit');
                }else{
                    var editBtn = $(handleObj).parent().find('.presentation-name-controls-edit');
                }
                if(typeof  value != "undefined" && value != ""){
                    $(handleObj).hide();
                    $(editBtn).show();
                    $(handleObj).parent().parent().find(element).html(value);
                }else{
                    alert('Empty value!');
                }
            },

            showPresentation: function(handleObj){
               methods.hidePresentation($(handleObj).parent().parent().parent().find('.hide-presentation'));
               $(handleObj).parent().parent().find('.presentation-slides').slideDown(200,function(){
                   $(handleObj).removeClass('show-presentation').addClass('hide-presentation');
               });
            },

            hidePresentation: function(handleObj){
                $(handleObj).parent().parent().find('.presentation-slides').slideUp(200,function(){
                    $(handleObj).parent().parent().find('.presentation-name').css('background-color','#d7d7d7');
                    $(handleObj).removeClass('hide-presentation').addClass('show-presentation');
                });
            },

            showAll: function(handleObj){
                $(handleObj).parent().parent().find('.bottom').slideDown(500,function(){
                    $(handleObj).removeClass('show-all').addClass('hide-all');
                });
            },

            hideAll: function(handleObj){
                $(handleObj).parent().parent().find('.bottom').slideUp(500,function(){
                    $(handleObj).removeClass('hide-all').addClass('show-all');
                });
            },

            nextSlide: function(handleObj){

            },

            prevSlide: function(handleObj){

            },

            pauseSlide: function(handleObj){

            },

            resumeSlide: function(handleObj){

            },

            changeSlide: function(handleObj){
                alert(234234);
            }
        };
        methods.init(settings, presentationObj);

        $('.accordion-container').on("click",settings['createButton'],function(){
            if(typeof $(this).parent().find(' :input').val() != "undefined"
                && $(this).parent().find(' :input').val() != ""){
                var container = $(this).parent().parent().find('.presentation-container').find('.slides');
                methods.add($(this).parent().find(' :input').val(), container);
                $(this).parent().find(' :input').val('');
            }else{
                alert('Empty presentation slide Name!');
            }
            return false;
        });

        $('.presentation-content').on("click",settings['createNewpresentation'],function(){
            if(typeof $(this).parent().find(' :input').val() != "undefined"
                && $(this).parent().find(' :input').val() != ""){
                var container = $(this).parent().parent().find('.accordion-container');
                console.log(container);
                methods.addPresentation($(this).parent().find(' :input').val(), container);
                $(this).parent().find(' :input').val('');
            }else{
                alert('Empty Slide Name!');
            }
            return false;
        });

        $(".presentation-content").on("click",".presentation-slide-controls-delete",function(){
            if( confirm('Are you sure you want to delete this slide?') ){
                methods.deleteSlide(this,'.presentation-slide-text');
            }
            return false;
        }).on("click",'.presentation-slide-controls-edit',function(){
            methods.edit(this,'.presentation-slide-text');
            return false;
        }).on("click",'.presentation-slide-controls-done',function(){
            methods.done(this,'.presentation-slide-text');
            return false;
        }).on("click",".presentation-name-controls-delete",function(){
            if( confirm('Are you sure you want to delete this presentation?') ){
                methods.deleteSlide(this,'.name');
            }
            return false;
        }).on("click",'.presentation-name-controls-edit',function(){
            methods.edit(this,'.name');
            return false;
        }).on("click",'.presentation-name-controls-done',function(){
            methods.done(this,'.name');
            return false;
        });

        $('.presentation-content').on('click','.presentation-name',function(){
            if($(this).find('.hide-presentation').length){
                methods.hidePresentation($(this).find('.hide-presentation'));
                $(this).css('backgroundColor','#d7d7d7');
            }else{
                methods.showPresentation($(this).find('.show-presentation'));
                $(this).css('backgroundColor','#EFEFEF');
            }
            return false;
        });

        $('.presentation-content').on('click','.top',function(){
            if($(this).find('.hide-all').length){
                methods.hideAll($(this).find('.hide-all'));
                $(this).css('backgroundColor','#d7d7d7');
            }else{
                methods.showAll($(this).find('.show-all'));
                $(this).css('backgroundColor','#EFEFEF');
            }
            return false;
        });

        $('.presentation-content').on('click',':input',function(){
            return false;
        })

        $('.presentation-content').on('mouseenter','.controls',function(){
            $(this).animate({ opacity: 0.7 }, 200);
        }).on('mouseleave','.controls',function(){
            $(this).animate({ opacity: 0.1 }, 200);
        });

        $( ".controls-slider" ).slider({
            slide: methods.changeSlide($(this))
            //change: methods.changeSlide($(this))
        });
    };
})(jQuery);